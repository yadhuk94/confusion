$(document).ready(function() {
    $('#mycarousel').carousel({ interval: 2000 });
    $('#carouselButton').click(function() {
        if ($('#carouselButton').children('span').hasClass('fa-pause')) {
            $('#mycarousel').carousel('pause');
            $('#carouselButton').children('span').removeClass('fa-pause');
            $('#carouselButton').children('span').addClass('fa-play');
        }
        else if ($('#carouselButton').children('span').hasClass('fa-play')) {
            $('#mycarousel').carousel('cycle');
            $('#carouselButton').children('span').removeClass('fa-play');
            $('#carouselButton').children('span').addClass('fa-pause');
        }                 
    });
    $("#myModal").on("click", function(){
        $('#loginModal').modal("toggle");
    });
    $("#closeButton").on("click", function(){
        $('#loginModal').modal("hide");
    });
    $("#closeIcon").on("click", function(){
        $('#loginModal').modal("hide");
    });
    $("#reserveToggle").on("click", function(){
        $('#reserveModal').modal("toggle");
    });
    $("#closeButtonR").on("click", function(){
        $('#reserveModal').modal("hide");
    });
    $("#closeIconR").on("click", function(){
        $('#reserveModal').modal("hide");
    });
})